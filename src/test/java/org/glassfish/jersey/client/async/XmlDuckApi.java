package org.glassfish.jersey.client.async;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("xml")
public interface XmlDuckApi {

    @POST
    @Path("valid")
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_XML})
    XmlDuck postValid(@Valid XmlDuck entity);
}
