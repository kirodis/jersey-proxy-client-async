package org.glassfish.jersey.client.async;

import io.vavr.Function1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

class XmlResourceTest extends AsyncWebResourceTest<XmlDuckApi> {
    
    XmlResourceTest() {
        super(XmlDuckApi.class, XmlDuckResource.class);
    }

    @Test
    void postValid() throws ExecutionException, InterruptedException {
        Function1<XmlDuck, CompletableFuture<XmlDuck>> subj = resource.init1(XmlDuckApi::postValid);
        XmlDuck result = subj.apply(new XmlDuck("Scrouge")).get();
        Assertions.assertEquals("Scrouge McDuck", result.name);
    }
}
