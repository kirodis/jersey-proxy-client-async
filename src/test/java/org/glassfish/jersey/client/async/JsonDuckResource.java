package org.glassfish.jersey.client.async;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

public class JsonDuckResource implements JsonDuckApi {
    @Override
    public String quack(int count) {
        return io.vavr.collection.List.tabulate(count, i -> "quack").collect(Collectors.joining(" "));
    }

    @Override
    public String quack(String name, int count) {
        return "Duck '" + name + "' quacks " + count + " times";
    }

    @Override
    public String getPlain() {
        return "quack quack";
    }

    @Override
    public String getByNameHeader(String name) {
        return "Duck '" + name + "' is here";
    }

    @Override
    public String getByNameHeaderList(List<String> items) {
        return "List: " + String.join(",", items);
    }

    @Override
    public String getByNameHeaderSet(Set<String> items) {
        return "Set: " + String.join(",", items);
    }

    @Override
    public String getByNameHeaderSortedSet(SortedSet<String> items) {
        return "SortedSet: " + String.join(",", items);
    }


    @Override
    public String getByNameCookie(String name) {
        return "cookie " + name;
    }

    @Override
    public String getByNameMatrix(String name) {
        return "matrix " + name;
    }
}
