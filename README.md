# Asynchronous jersey proxy client (jersey-proxy-client-async)

## Overview

This project is an alternative to jersey-proxy-client that allows to use JAX-RS interface classes that are declared in
a synchronous way (e.g. without returning the java.util.concurrent.CompletionStage)

Historically, publishing jars with JAX-RS interface delarations and consuming them on client side using 
jersey-proxy-client (or apache-cxf) has become a popular way of managing JAX-RS API's.
Support for CompletionStage took quite some time to arrive in JAX-RS standard and at the time this 
text was written did not have wide adoption for above mentioned case. A number of factors has contributed to this:
- Designing new API's often happens under time pressure and lack of requirements, so no second thought is given 
regarding asynchronous processing.
- API's are usually owned by teams responsible for the server. They often have limited motivation to invest into things 
that are useful for client side only, especially in those they will have to maintain.
- If there are non-Java consumers, the contract for managing the API will be wadl, with JAXB used to generate either
classes (schema-first) or schema (code-first) approach. Neither of these options supports CompletionStage or Response 
as method return value, and such support is unlikely to arrive in near future.     
- Even if all consumers are Java, some of them might be synchronous. They will be naturally unhappy to see async API's
and deal with the related overhead. Maintaining both synchronous and asynchronous API is clearly an overhead for the 
server team and is prone to human errors, as there is hardly any way to avoid manual cross-checking. 

The above factors will stay in play for a while, which means the industry will keep seeing even new JAX-RS API's 
published without async support for a while. This project attempts to remedy this, by providing a way to convert a 
synchronous API into a one that returns java.util.concurrent.Future. 

## Usage

This library is currently in early development stage and is not published in any maven repository.
You can copy the classes in the source folder and use them in your application.

For example, given the following interface 

    import javax.ws.rs.GET;
    import javax.ws.rs.Path;
    import javax.ws.rs.PathParam;
    import javax.ws.rs.Produces;
    import javax.ws.rs.core.MediaType;
    
    @Path("some")
    public interface SomeApi {
        @GET
        @Path("get/{id}")
        @Produces(MediaType.TEXT_PLAIN)
        String getStuff(@PathParam("id") int id);
    }

A service that takes advantage of asynchronous calls, might look like: 

    import io.vavr.Function1;
    import org.glassfish.jersey.client.async.AsyncWebResource;
    
    import javax.ws.rs.client.WebTarget;
    import java.text.MessageFormat;
    import java.util.concurrent.CompletableFuture;
    
    public class AsyncStuffService {
        private final Function1<Integer, CompletableFuture<String>> getStuff;
    
        public AsyncStuffService(WebTarget webTarget) {
            AsyncWebResource<SomeApi> resource = AsyncWebResource.of(webTarget, SomeApi.class);
            getStuff = resource.init1(SomeApi::getStuff);
        }
    
        public CompletableFuture<String> getTwo(int idHead, int idTail) {
            CompletableFuture<String> head = getStuff.apply(idHead);
            CompletableFuture<String> tail = getStuff.apply(idTail);
            return head.thenCombine(tail, (a, b) -> MessageFormat.format("Got stuff: {0} and {1}.", a, b));
        }
    }
    
## Design notes

Since our library needs to produce function with a reasonably arbitrary number of arguments, and JDK only has 
java.util.function.Function and java.util.function.BiFunction on board, we needed to use some other functional primitive 
classes. Maintaining one ourselves was never part of the plan, so the choice was between available solutions.
Since switching between functional primitives from different libraries is generally as simple as adding '::apply',
the research here wasnt really a hardcore one, but a basic analysis was done against the following requirements:
- No conflicts in naming with JDK classes
- The max number of arguments for primitives
- Concise syntax 
- Presence of methods for partial application on the primitive itself

The following options were considered: 
- functionalJava: 8 args, kind of steep learning curve, very concise (but sometime cryptic) syntax, no partial   
- jOOλ: 16 args, kind of steep learning curve, concise (but sometimes cryptic) syntax, has partial 
- RxJava: 9 args, no partial application, conflicts in naming.  
- vavr.io: 8 args, reasonably concise syntax, has partial. 

After trying to build an implementation with jOOλ and vavr.io, it was discovered that when it comes to not just API, but
to using the library inside the code, collections from vavr.io were found more practical in our context and became a tie
breaker.     
          